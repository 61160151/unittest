//A3
const { describe } = require('mocha');
const assert = require('assert')
const { checkEnableTime } = require('../JobPosting')

describe('JobPosting', () => {
  it('ในระหว่างเวลารับสมัคร', () => {
    //Arrage
    const start = new Date(2021, 1, 31)
    const end = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 3)
    const expectedResult = true;
    //Act
    const actualResult = checkEnableTime(start, end, today)
    //Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('ก่อนเวลารับสมัคร', () => {
    //Arrage
    const start = new Date(2021, 1, 31)
    const end = new Date(2021, 2, 5)
    const today = new Date(2021, 1, 30)
    const expectedResult = false;
    //Act
    const actualResult = checkEnableTime(start, end, today)
    //Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('หลังเวลารับสมัคร', () => {
    //Arrage
    const start = new Date(2021, 1, 31)
    const end = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 6)
    const expectedResult = false;
    //Act
    const actualResult = checkEnableTime(start, end, today)
    //Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('เริ่มรับสมัคร', () => {
    //Arrage
    const start = new Date(2021, 1, 31)
    const end = new Date(2021, 2, 5)
    const today = new Date(2021, 1, 31)
    const expectedResult = true;
    //Act
    const actualResult = checkEnableTime(start, end, today)
    //Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('วันสุดท้ายของการรับสมัคร', () => {
    //Arrage
    const start = new Date(2021, 1, 31)
    const end = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 5)
    const expectedResult = true;
    //Act
    const actualResult = checkEnableTime(start, end, today)
    //Assert
    assert.strictEqual(actualResult, expectedResult)
  })
})